import bloomclient
import asyncio

async def ping(port):
    cli = bloomclient.BloomClient()
    await cli.connect(port)
    await cli.send("ADD", bloomclient.tokenize("ping\n"))
    await cli.send("FORWARD")
    tokens = await cli.send("TOKENS")
    print(bloomclient.detokenize(tokens[:, 0]))
    await cli.send("CLOSE")
    cli.close()

asyncio.run(ping(1330))